//
//  SimpleKMLLineString.m
//  SimpleKML
//
//  Created by Markus on 09.08.12.
//
//

#import "SimpleKMLLineStringTest.h"
#import "SimpleKML.h"
#import "SimpleKMLDocument.h"
#import "SimpleKMLMultiGeometry.h"
#import "SimpleKMLPlacemark.h"
#import "SimpleKMLLineString.h"
#import <CoreLocation/CoreLocation.h>
#import <OCMock/OCMock.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

@implementation SimpleKMLLineStringTest


- (void)setUp
{
    [super setUp];
    
    
    // create coordinates to check against
    
    
    self.xmlNodeMock    = [OCMockObject mockForClass:[CXMLElement class]];

    
    // Set-up code here.
     NSString* testKmlFilePath = [[NSBundle bundleForClass:[SimpleKMLLineStringTest class]] pathForResource:@"SimpleKMLLineStringTest" ofType:@"kml"];
    
    self.kml = [SimpleKML KMLWithContentsOfFile:testKmlFilePath error:NULL];

}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testDefault
{
    NSError* error;
  
    NSString* coordinateString = @"-122.681944,45.52000000,32.000000\n-43.1963890,-22.9083330,6.0000000\n28.97601800,41.01224000,32.000000\n-21.9333330,64.13333300,13.000000\n-122.681944,45.52000000,32.000000>";


    id xmlChildMock = [OCMockObject mockForClass:[CXMLElement class]];
    [(CXMLNode*)[[xmlChildMock stub] andReturn:@"coordinates"] name];
    [(CXMLNode*)[[xmlChildMock stub] andReturn:coordinateString] stringValue];

    [[[self.xmlNodeMock stub] andReturn:@[xmlChildMock]] children];
    [[[self.xmlNodeMock stub] andReturn:@"xmlString"] XMLString];
    [[[self.xmlNodeMock stub] andReturn:nil] attributeForName:@"id"];

    
    
    SimpleKMLLineString* lineString = [[SimpleKMLLineString alloc] initWithXMLNode:self.xmlNodeMock sourceURL:[NSURL URLWithString:@"Dummy"] error:&error];
        
    assertThat(lineString, notNilValue());
    assertThat(lineString.coordinates, hasCountOf(5));
    
    CLLocationCoordinate2D location2D;
    location2D.latitude= 45.52000000;
    location2D.longitude=-122.681944;
    
    CLLocation* location = [[CLLocation alloc] initWithCoordinate:location2D altitude:32.000000 horizontalAccuracy:0 verticalAccuracy:0  timestamp:[NSDate date]];
    
    assertThat([NSNumber numberWithDouble:[[lineString.coordinates objectAtIndex:0] altitude]], equalToDouble(location.altitude));
    assertThat([NSNumber numberWithDouble:[[lineString.coordinates objectAtIndex:0] coordinate].latitude], equalToDouble(location.coordinate.latitude));
    assertThat([NSNumber numberWithDouble:[[lineString.coordinates objectAtIndex:0] coordinate].longitude], equalToDouble(location.coordinate.longitude));

}



@end
