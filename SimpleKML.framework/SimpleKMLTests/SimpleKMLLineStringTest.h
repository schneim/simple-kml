//
//  SimpleKMLLineString.h
//  SimpleKML
//
//  Created by Markus on 09.08.12.
//
//

#import <SenTestingKit/SenTestingKit.h>
#import "SimpleKML.h"

@interface SimpleKMLLineStringTest : SenTestCase


@property SimpleKML*            kml;
@property NSMutableArray*       coordinates;
@property id                    xmlNodeMock;
@end
