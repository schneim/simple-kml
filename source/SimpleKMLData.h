//
//  SimpleKMLData.h
//
//  Created by Watanabe Toshinori on 12/21/10.
//  Copyright 2010 FLCL.jp. All rights reserved.
//
// http://code.google.com/apis/kml/documentation/kmlreference.html#data

#import "SimpleKMLObject.h"

@interface SimpleKMLData : SimpleKMLObject
{
	NSString *name;
	NSString *value;
	NSString *displayName;
}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, strong) NSString *displayName;

@end
