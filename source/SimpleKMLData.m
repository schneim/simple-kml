//
//  SimpleKMLData.m
//
//  Created by Watanabe Toshinori on 12/21/10.
//  Copyright 2010 FLCL.jp. All rights reserved.
//

#import "SimpleKMLData.h"
#import "CXMLElement.h"

@implementation SimpleKMLData
@synthesize name;
@synthesize value;
@synthesize displayName;

- (id)initWithXMLNode:(CXMLNode *)node sourceURL:sourceURL error:(NSError **)error
{
    self = [super initWithXMLNode:node sourceURL:sourceURL error:error];
    
    if (self != nil)
    {
		value = nil;
		displayName = nil;

		name  = [[[((CXMLElement *)node) attributeForName:@"name"] stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		
        for (CXMLNode *child in [node children])
        {
			if ([[child name] isEqualToString:@"value"])
                value = [[child stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

			else if ([[child name] isEqualToString:@"displayName"])
                displayName = [[child stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		}
	}
	
	return self;
}


@end

