//
//  SimpleKMLColor.m
//  SimpleKML
//
//  Created by Markus on 08.01.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SimpleKMLColor.h"

#ifdef __MAC_OS_X_VERSION_MAX_ALLOWED

@implementation  SimpleKMLColor (SimpleKML_Color)

+ (SimpleKMLColor *)colorWithRed:(CGFloat)r green:(CGFloat)g blue:(CGFloat)b alpha:(CGFloat)a
{
    return [self colorWithCIColor:[CIColor colorWithRed:r green:g blue:b alpha:a]];
}




@end




#endif