//
//  SimpleKMLExtendedData.m
//
//  Created by Watanabe Toshinori on 12/21/10.
//  Copyright 2010 FLCL.jp. All rights reserved.
//

#import "SimpleKMLExtendedData.h"
#import "SimpleKMLData.h"

@implementation SimpleKMLExtendedData

@synthesize data;

- (id)initWithXMLNode:(CXMLNode *)node sourceURL:sourceURL error:(NSError **)error
{
    self = [super initWithXMLNode:node sourceURL:sourceURL error:error];
    
    if (self != nil)
    {
		self.data = [NSMutableArray array];
		
        for (CXMLNode *child in [node children])
        {
            Class dataClass = NSClassFromString([NSString stringWithFormat:@"SimpleKML%@", [child name]]);
            
            if (dataClass)
            {
                id thisDataClass = [[dataClass alloc] initWithXMLNode:child sourceURL:sourceURL error:NULL];
                
                if (thisDataClass && [thisDataClass isKindOfClass:[SimpleKMLData class]])
                {
					[data addObject:thisDataClass];
				}
			}
		}
	}
	
	return self;
}


@end
