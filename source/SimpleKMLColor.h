//
//  SimpleKMLColor.h
//  SimpleKML
//
//  Created by Markus on 08.01.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED

#import <UIKit/UIKit.h>

#define SimpleKMLColor UIColor

#endif


#ifdef __MAC_OS_X_VERSION_MAX_ALLOWED

#import <Quartz/Quartz.h>
#import <Cocoa/Cocoa.h>

#define SimpleKMLColor NSColor

@interface SimpleKMLColor (SimpleKML_Color)


+ (SimpleKMLColor *)colorWithRed:(CGFloat)r green:(CGFloat)g blue:(CGFloat)b alpha:(CGFloat)a;

@end



#endif






