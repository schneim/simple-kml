//
//  SimpleKMLExtendedData.h
//
//  Created by Watanabe Toshinori on 12/21/10.
//  Copyright 2010 FLCL.jp. All rights reserved.
//
// http://code.google.com/apis/kml/documentation/kmlreference.html#extendeddata

#import "SimpleKMLObject.h"

@class SimpleKMLData;

@interface SimpleKMLExtendedData : SimpleKMLObject
{
	NSMutableArray *data;
}

@property (nonatomic, strong) NSMutableArray *data;

@end
